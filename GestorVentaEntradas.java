import Cine.Cine;
import Cine.Sala;
import interfazusuario.VentanillaVirtualUsuario;

import java.awt.event.KeyEvent;
import java.util.Scanner;

public class GestorVentaEntradas {

    private static String[][] horarios = {{"15:00", "17:00", "19:00", "21:00"}, {"15:30", "17:30", "19:30", "21:30"}};
    private static String[] titulos = {"Detective Conan", "Una peli de mierda", "Otra peli de mierda"};
    private static int filas = 9;
    private  static int columnas = 7;
    private static int[] pasillos = {2, 5} ;
    private static Sala[] salas = {new Sala(titulos[0],horarios[0],filas,columnas, pasillos), new Sala(titulos[1],horarios[1],filas,columnas, pasillos)};


    public static void main(String[] args) {

        Cine JulentxiVision = new Cine("julentxiVision", salas);
        int cliente = 0; boolean stop = false;
        Scanner input = new Scanner(System.in);

        do{
            System.out.print("Pulse cualquier letra para proceder \t :");
            String proceder = input.nextLine();

            if(!proceder.equals("exit")){
                cliente += 1;
                VentanillaVirtualUsuario ventanilla = new VentanillaVirtualUsuario(JulentxiVision, cliente);
                ventanilla.abrir();
            } else {
                stop = true;
            }

        }while(!stop);

    }

}
