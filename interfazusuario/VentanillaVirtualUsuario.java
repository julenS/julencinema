package interfazusuario;

import Cine.Cine;
import Cine.ButacasContiguas;
import interfazusuario.menu.Menu;

import java.util.Scanner;

public class VentanillaVirtualUsuario {

     private Cine cine;
     private int cliente;
     private static String[][] opciones = {
             {"EXIT", "PELICULAS Y SESIONES"},
             {"GO BACK","VER ESTADO SESION", "COMPRAR ENTRADA(S)", "RECOMENDAR BUTACAS CONTIGUAS", "RECOGER ENTRADA" },
             {"CONFIRMAR", "CANCELAR"}
     };
     private int salaSelec;
     private int sesionSelec;

     public VentanillaVirtualUsuario(Cine cine, int cliente){
         this.cine = cine;
         this.cliente = cliente;
     }

     public void abrir() {
         System.out.println("////////////////////////////////////////////////////////////////////");
         System.out.println("\t\t Bienvenido al cine:" + cine.getNombre() );
         System.out.println("////////////////////////////////////////////////////////////////////");
         Menu principal = new Menu(opciones[0]);
         Menu tienda = new Menu(opciones[1]);
         Menu confirmar = new Menu(opciones[2]);

         int principalCallback = principal.displayMenu();

         switch (principalCallback) {
             case 1 :{
                 System.out.println("Esperamos volver a verte \t Cliente \t" + this.cliente + "\t adiosito.");
                 break;
             }
             case 2 : {
                 int[] yoloSwag = selectPeliSesion();
                 salaSelec = yoloSwag[0];
                 sesionSelec = yoloSwag[1];
                 segundoMenu();
                 break;
             }
         }
     }

     private void segundoMenu() {
         Menu segundo = new Menu(opciones[1]);
         int segundoCallBack = segundo.displayMenu();

         switch (segundoCallBack) {
             case 1: {
                    abrir();
                    break;
             }
             case 2: {
                estadoSesion(salaSelec, sesionSelec);
                segundoMenu();
                break;
             }
             case 3 : {
                 comprarEntrada();
                 break;

             }
             case 5 : {
                    recogerEntradas(this.salaSelec, this.sesionSelec);
                    break;
             }
             case 4 : {
                    recomendarYComprar();
                    break;
             }

         }
     }

     private void estadoSesion(int sala, int sesion) {
         int columnas = 0;
         int butacasLibres = cine.getButacasDisponiblesSesion(sala, sesion);
         char[][] estado = cine.getEstadoSesion(sala, sesion);

         System.out.println("Actualmente se encuentran \t" + butacasLibres + "\t butacas libres. \n");

         for (int k = 0; k < estado[0].length; k++){
             if (estado[0][k] != ' ') {
                 columnas += 1;
                 System.out.print((columnas) + " ");
             } else {
                 System.out.print("  ");
             }
         }
         System.out.println(" \n ");

         for (int i = 0; i < estado.length; i++){
             for (int j = 0; j< estado[i].length; j++){
                    System.out.print(estado[i][j] + " ");
             }
             System.out.println("\t"+ (i+1) + " ");

         }
         waitUntil();
         segundoMenu();

     }

     private int[] selectPeliSesion(){
         Menu salas = new Menu(cine.getPeliculas());
         int sala = salas.displayMenu();
         Menu sesiones = new Menu(cine.getHorasDeSesionesDeSala(sala));
         int sesion = sesiones.displayMenu();

         return new int[] {sala, sesion};
     }

     /* private int mostrarPelis(){
         Scanner input = new Scanner(System.in);
        String[] peliculas = cine.getPeliculas();
        int pelicula = 0;
        for (int i = 0; i < peliculas.length; i++){
            System.out.println(i+1 + "\t\t " + peliculas[i]);
        }
        do{
            int selec = input.nextInt();
            if (selec > 0 && selec <= peliculas.length + 1){
                pelicula = selec;
            }
        }while(pelicula <= 0 || pelicula > peliculas.length + 1);

        return pelicula;
     }

     private int mostrarSesiones(int sala){
         Scanner input = new Scanner(System.in);
         int sesion = 0;
         String[] sesiones = cine.getHorasDeSesionesDeSala(sala);

         for (int i = 0; i < sesiones.length; i++){
             System.out.println(i+1 + "\t\t " + sesiones[i]);
         }
         do{
             int selec = input.nextInt();
             if (selec > 0 && selec <= sesiones.length + 1){
                 sesion = selec;
             }
         }while(sesion <= 0 && sesion >= sesiones.length + 1);

         return sesion;

     } */

     private int pedirValor(String dato) {
         Scanner input = new Scanner(System.in);
         int output = 0;
         System.out.println("Por favor introduzca " + dato + " >>" );

         do{
             output = input.nextInt();
         }while (output == 0);
         return output;
     }

     private void recogerEntradas(int sala, int sesion){
         int id = pedirValor("entradaId");
         String entrada = cine.recogerEntradas(id, sala, sesion);
         System.out.println(entrada);
     }


     private void comprarEntrada(){
         int fila = pedirValor("fila");
         int columna = pedirValor("columna");
         int idEntrada;

         if (pedirConfirmacion()){
             cine.comprarEntrada(this.salaSelec, this.sesionSelec, fila, columna);
             idEntrada = cine.getIdEntrada(this.salaSelec, this.sesionSelec, fila, columna);
             System.out.println("Este es el identificador de su entrada \t" + idEntrada);
         }
     }



     private void recomendarYComprar() {
           int noButacas = pedirValor("cantidad butacas");
           int idEntrada;
           ButacasContiguas butacas = cine.recomendarButacasContiguas(noButacas, this.salaSelec, this.sesionSelec );
           System.out.println("--Fila" + butacas.getFila()  + "--Butacas" + butacas.getColumna()  + "hasta butaca " + (butacas.getColumna() + (noButacas-1)) + "\n");

           if(pedirConfirmacion()){
                cine.comprarEntradasRecomendadas(this.salaSelec, this.sesionSelec, butacas);
                idEntrada = cine.getIdEntrada(this.salaSelec, this.sesionSelec, butacas.getFila() , butacas.getColumna());
                System.out.println(idEntrada);
           }


     }

     private boolean pedirConfirmacion() {
         Scanner input = new Scanner(System.in);

         System.out.println("¿Esta seguro de querer realizar esta operacion? yes/no");

         do{
            String respuesta = input.nextLine();
            if(respuesta.equals("y") || respuesta.equals("Y") || respuesta.equals("yes") || respuesta.equals("YES")){
                return true;
            } else if(respuesta.equals("n") || respuesta.equals("N") || respuesta.equals("no") || respuesta.equals("NO")){
                return false;
            }
         }while(true);

     }

     private void waitUntil() {
         Scanner input = new Scanner(System.in);
         String endLoop ="yesplease";

         do {
             endLoop = input.nextLine();
         }while(endLoop.equals("yesplease"));
     }
}



