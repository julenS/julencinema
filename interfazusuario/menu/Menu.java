package interfazusuario.menu;

import java.util.Scanner;

public class Menu {

    private String[] opciones;

    public Menu(String[] opciones) {
        this.opciones = opciones;
    }


    public int displayMenu(){
        Scanner input = new Scanner(System.in);
        int callBack = -1;
        System.out.print("Seleccione una opción : \n");
        for (int i = 0; i < opciones.length; i++){
            System.out.println(i+1 + "\t>>\t\t" +  opciones[i]);
        }

        do{
            int newCallBack = input.nextInt();
            if((newCallBack > 0) && (newCallBack < opciones.length + 1)){
                callBack = newCallBack;
            }
        }while ( callBack <= 0 || callBack > opciones.length + 1);
        return callBack;
    }
}
