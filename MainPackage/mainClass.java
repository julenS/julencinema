package MainPackage;

import Cine.Cine;
import Cine.Sala;
import Cine.Sesion;

import java.util.Arrays;

/* ///////////////////////////////////////////////
        PRUEBAS
   ///////////////////////////////////////////////
 */
public class mainClass {

    public static void  main(String[] args){
        String[] horaSesiones = {"15:55", "18:30", "21:10", "23:45"};
        int[] pasillos = {2, 5};

        Sala horaDeAventuras = new Sala("Hora de Aventuras", horaSesiones, 9, 7, pasillos );
        Sala Conan = new Sala("Detective Conan", horaSesiones, 9, 7, pasillos );

        Sala[] salas = {horaDeAventuras, Conan};
        Cine micasa = new Cine("micasa",salas);
        System.out.println(Arrays.toString(micasa.getPeliculas()));
        System.out.println(Arrays.toString(micasa.getHorasDeSesionesDeSala(1)));
        System.out.println(micasa.getButacasDisponiblesSesion(1,3));
        System.out.println(Arrays.deepToString(micasa.getEstadoSesion(1, 3)));
        micasa.comprarEntrada(1, 3, 1, 4);
        System.out.println(Arrays.deepToString(micasa.getEstadoSesion(1, 3)));
        micasa.comprarEntrada(1, 3, 2, 3);
        System.out.println(Arrays.deepToString(micasa.getEstadoSesion(1, 3)));
        System.out.println(micasa.getButacasDisponiblesSesion(1,3));
    }
}
