package Cine;

import java.util.ArrayList;
import java.util.Arrays;

public class Cine {
    private String nombre;
    private ArrayList<Sala> salas;

    public Cine(String nombre, Sala[] salas){
        this.nombre = nombre;
        this.salas = new ArrayList<Sala>(Arrays.asList(salas));
    }


    public void comprarEntrada(int sala, int sesion, int fila, int columna){
        salas.get(sala -1).comprarEntrada(sesion, fila, columna);
    }
    public void comprarEntradasRecomendadas(int sala, int sesion, ButacasContiguas butacas){
        salas.get(sala -1).comprarEntradasRecomendadas(sesion, butacas);
    }
    public int getButacasDisponiblesSesion(int sala, int sesion) {
        return salas.get(sala -1).getButacasDisponiblesSesion(sesion);
    }
    public char[][] getEstadoSesion(int sala, int sesion) {
        return salas.get(sala -1).getEstadoSesion(sesion);
    }
    public String[] getHorasDeSesionesDeSala(int sala) {
        return salas.get(sala -1).getHoraDeSesionesDeSala();
    }
    public int getIdEntrada(int sala, int sesion, int fila, int columna) {
        return salas.get(sala -1).getIdntrada(sesion, fila, columna);
    }
    public String[] getPeliculas() {
        int cantPeliculas = this.salas.size();
        System.out.println(cantPeliculas);
        String[] peliculas = new String[cantPeliculas];

        for (int i = 0; i < cantPeliculas; i++) {
            peliculas[i] = this.salas.get(i).getPelicula();
        }

        return peliculas;
    }
    public String recogerEntradas(int id, int sala, int sesion) {
        return salas.get(sala -1).recogerEntradas(id, sesion);
    }

    public String getNombre() {
        return nombre;
    }

    public ButacasContiguas recomendarButacasContiguas(int noButacas, int sala, int sesion) {
        return salas.get(sala -1).recomendarButacasContiguas(noButacas, sesion);
    }

}
