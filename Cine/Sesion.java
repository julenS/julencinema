package Cine;

import java.util.ArrayList;

public class Sesion {
    private int asientosDisponibles;
    private int[][] estadoAsientos;
    private String hora;
    private int sigIdCompra;



    Sesion(String hora, int filas, int columnas, int[] columnasSinAsiento ){
        this.hora = hora;
        this.sigIdCompra = 1;
        int asientosTotales = 0;
        int[][] asientos = new int[filas][columnas];

        for (int i = 0; i < asientos.length; i++){
            for (int k = 0; k < asientos[i].length; k++){
                for (int pasillo: columnasSinAsiento) {
                    if (k == pasillo) {
                        asientos[i][k] = -1;
                        break;
                    }
                }
                if (asientos[i][k] != -1) {
                    asientosTotales += 1;
                }
            }
        }
        this.estadoAsientos = asientos;
        this.asientosDisponibles = asientosTotales;

       /* for (int i = 0; i < asientos[1].length; i++) {
            if (asientos[1][i] != -1){
                asientosTotales += filas;
            }
        } */

        
    }


    /*
        Busca butacas contíguas en la fila solicitada,
        guarda parametro posicional de primera butaca libre, lo resetea en caso de no haber 3 libres seguidas
     */
    public ButacasContiguas buscarButacasContiguasEnFila(int fila, int noButacas){
        int butacasSeguidas = 0;
        int columnaInicial = -1;

         for(int i = 0; i < this.estadoAsientos[fila -1].length && butacasSeguidas != noButacas; i++){
            if (this.estadoAsientos[fila -1][i] == 0){
                butacasSeguidas += 1;
                if (butacasSeguidas == 1){
                    columnaInicial = i;
                }
            } else if (this.estadoAsientos[fila -1][i] > 0){
                butacasSeguidas = 0;
                columnaInicial = -1;
            }
        }
         if(butacasSeguidas == noButacas){
             return new ButacasContiguas(fila, columnaInicial  , noButacas);
         } else {
             return null;
         }

        // bucle reverso

        /*for(int i = this.estadoAsientos[fila -1].length; i > noButacas -1 ; --i){
            if (this.estadoAsientos[fila -1][i] == 0){
                butacasSeguidas += 1;

                if (butacasSeguidas == noButacas){
                    columnaInicial = i;
                    ButacasContiguas resultado = new ButacasContiguas(fila, columnaInicial + 1 , noButacas);
                    return resultado;
                }
            } else if (this.estadoAsientos[fila -1][i] > 0){
                butacasSeguidas = 0;
            }
        } */


    }
    // calculo cuantos pasillos hay hasta llegar a la columna selesccionada
    private int calcularNumeroPasillosHastaColumna(int fila, int columna){
        int pasillos = 0;

        for(int k = 0; k < columna + pasillos; k++) {
            if( this.estadoAsientos[fila][k] == -1){
                ++ pasillos;
            }
        }
        return pasillos;
    }
    // Guardo sigIdCompra en la butaca comprada + actualizo sigIdCompra y asientosDisponibles

    public void comprarEntrada(int fila, int columna) {
        int pasillos = calcularNumeroPasillosHastaColumna(fila -1, columna);

        if(this.estadoAsientos[fila -1][(columna -1) + pasillos] == 0){
            this.estadoAsientos[fila -1][(columna -1) + pasillos] = this.sigIdCompra;
            this.sigIdCompra ++;
            this.asientosDisponibles --;
        } else {
            System.out.println("Esa butaca no está disponible ");
        }

    }

    public void comprarEntradasRecomendadas(ButacasContiguas butacas) {
        int fila = butacas.getFila();
        int columna = butacas.getColumna();
        int noButacas = butacas.getNoButacas();
        int pasillos = calcularNumeroPasillosHastaColumna(fila -1 , (columna));
        int loopPasillos = 0;


        for (int i = 0; i  < noButacas + loopPasillos ; i++)
        {
            if (this.estadoAsientos[fila -1][((columna -1) + pasillos) + i] != -1){
                this.estadoAsientos[fila -1][((columna -1) + pasillos) + i] = this.sigIdCompra;
            } else {
                ++ loopPasillos;
            }

        }
        this.sigIdCompra += 1;
        this.asientosDisponibles -= noButacas;
    }

    public int getButacasDisponiblesSesion() {
       return this.asientosDisponibles;
    }

    /* Devuelvo estado estadoSesion cambiando los valores int:
        Libre(0): "O"
        Ocupada(): "X"
        Columna(-1): "  "
    */
    public char[][] getEstadosSesion() {
        int cantidadFilas = this.estadoAsientos.length;
        int cantidadColumnas = this.estadoAsientos[0].length;

        char[][] estadoSesionChar = new char[cantidadFilas][cantidadColumnas];

        for (int i = 0; i < this.estadoAsientos.length; i ++) {
            for (int k = 0; k < this.estadoAsientos[k].length; k++) {
                if (this.estadoAsientos[i][k] == 0) {
                    estadoSesionChar[i][k] = 'O' ;
                } else if (this.estadoAsientos[i][k] == -1) {
                    estadoSesionChar[i][k] = ' ';
                } else {
                    estadoSesionChar[i][k] = 'X';
                }
            }
        }
        return estadoSesionChar;
    }

    public String getHora(){
        return this.hora;
    }

    public int getIdEntrada(int fila, int columna){
        int pasillos = calcularNumeroPasillosHastaColumna(fila -1 , columna);
        return this.estadoAsientos[fila -1][columna -1 + pasillos];
    }

    public String recogerEntradas(int id){
        StringBuilder entrada = new StringBuilder();
        int pasillos;

        for (int i = 0; i < this.estadoAsientos.length; i++){
            pasillos = 0;
            for (int k = 0; k < this.estadoAsientos[i].length; k++){
                if (this.estadoAsientos[i][k] == id){
                    entrada.append("--fila:").append(i + 1).append(" --butaca:").append((k + 1) - pasillos ).append("\n");
                } else if (this.estadoAsientos[i][k] == -1) {
                    pasillos += 1;
                }
            }
        }
        return entrada.toString();

    }

    public ButacasContiguas recomendarBuatacasContiguas(int noButacas){
        int butacasSeguidas = 0;
        int columnaInicial = -1;
        int fila = -1;
        int putosPasillos;

        for (int i = this.estadoAsientos.length / 2 + 1; i < this.estadoAsientos.length && butacasSeguidas != noButacas; ++i){
            putosPasillos = 0; butacasSeguidas = 0;
            for (int k = 0; k < this.estadoAsientos[i].length && butacasSeguidas != noButacas; k++ ){
                if (this.estadoAsientos[i][k] == 0){
                    ++ butacasSeguidas;
                    if (butacasSeguidas == 1){
                        fila = i;
                        columnaInicial = k - putosPasillos;
                    }
                } else if (this.estadoAsientos[i][k] > 0){
                    butacasSeguidas = 0;
                } else if (this.estadoAsientos[i][k] == -1) {
                   ++ putosPasillos;
                }
            }
        }

        for(int i= this.estadoAsientos.length / 2; i >= 0 && butacasSeguidas != noButacas; --i){
            putosPasillos = 0; butacasSeguidas = 0;
            for (int k = 0; k < this.estadoAsientos[i].length && butacasSeguidas  != noButacas; k++ ) {
                if (this.estadoAsientos[i][k] == 0){
                    butacasSeguidas += 1;
                    if (butacasSeguidas == 1){
                        fila = i ;
                        columnaInicial = k - putosPasillos;
                    }
                } else if (this.estadoAsientos[i][k] > 0){
                    butacasSeguidas = 0;
                } else if (this.estadoAsientos[i][k] == -1) {
                    putosPasillos += 1;
                }
            }

        }

        if(butacasSeguidas == noButacas){
            return new ButacasContiguas(fila + 1, columnaInicial + 1, noButacas );
        } else {
            System.out.println("no hay " + noButacas + " butacas continuas disponibles, lo sentimos");
            return null;
        }
    }
}
