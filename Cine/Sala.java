package Cine;

import java.util.ArrayList;

public class Sala {
    private String pelicula;
    private ArrayList<Sesion> sesiones;

    public Sala(String pelicula, String[] horaSesiones, int filas, int columnas, int[] columnasSinAsiento){
        this.pelicula = pelicula;
        //System.out.println(pelicula);
        int cantidadSesiones = horaSesiones.length;
        this.sesiones = new ArrayList<Sesion>(cantidadSesiones);

        for (String horaSesion: horaSesiones) {
            Sesion  sesion = new Sesion(horaSesion, filas, columnas, columnasSinAsiento );
            this.sesiones.add(sesion);

        }
    }

    public void comprarEntrada(int sesion, int fila, int columna){
        sesiones.get(sesion -1).comprarEntrada(fila, columna);
    }

    public void comprarEntradasRecomendadas(int sesion, ButacasContiguas butacas) {
        sesiones.get(sesion -1).comprarEntradasRecomendadas(butacas);
    }

    public int getButacasDisponiblesSesion(int sesion) {
        return sesiones.get(sesion -1).getButacasDisponiblesSesion();
    }
    public char[][] getEstadoSesion(int sesion) {
        return sesiones.get(sesion -1).getEstadosSesion();
    }

    // probar si  el bucle es corrcto o necesita <=
    public String[] getHoraDeSesionesDeSala() {
        int cantSesiones = this.sesiones.size();
        String[] horaDeSesiones = new String[cantSesiones];

        for (int i = 0; i < cantSesiones; i++){
            String hora = this.sesiones.get(i).getHora();
            horaDeSesiones[i] = hora;
        }
        return horaDeSesiones;
    }

    public int getIdntrada(int sesion, int fila, int columna) {
        return sesiones.get(sesion -1).getIdEntrada(fila, columna);
    }

    public String getPelicula() {
        return this.pelicula;
    }

    public String recogerEntradas(int id, int sesion) {
        return sesiones.get(sesion -1).recogerEntradas(id);
    }
    public ButacasContiguas recomendarButacasContiguas(int noButacas, int sesion){
        return sesiones.get(sesion -1).recomendarBuatacasContiguas(noButacas);
    }

}
